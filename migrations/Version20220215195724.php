<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220215195724 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE equipment (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE equipment_order_item (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, equipment_id INTEGER NOT NULL, rental_order_id INTEGER NOT NULL, count INTEGER NOT NULL)');
        $this->addSql('CREATE INDEX IDX_7BE1EBD7517FE9FE ON equipment_order_item (equipment_id)');
        $this->addSql('CREATE INDEX IDX_7BE1EBD7BDF9740B ON equipment_order_item (rental_order_id)');
        $this->addSql('CREATE TABLE equipment_stock (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, equipment_id INTEGER NOT NULL, rental_station_id INTEGER NOT NULL, count INTEGER NOT NULL)');
        $this->addSql('CREATE INDEX IDX_A26F7784517FE9FE ON equipment_stock (equipment_id)');
        $this->addSql('CREATE INDEX IDX_A26F7784B1BD4FA3 ON equipment_stock (rental_station_id)');
        $this->addSql('CREATE TABLE rental_order (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, pickup_station_id INTEGER NOT NULL, return_station_id INTEGER NOT NULL, pickup_date DATE NOT NULL, return_date DATE NOT NULL)');
        $this->addSql('CREATE INDEX IDX_6EC21D7773279A0B ON rental_order (pickup_station_id)');
        $this->addSql('CREATE INDEX IDX_6EC21D77EA291807 ON rental_order (return_station_id)');
        $this->addSql('CREATE TABLE rental_station (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE equipment');
        $this->addSql('DROP TABLE equipment_order_item');
        $this->addSql('DROP TABLE equipment_stock');
        $this->addSql('DROP TABLE rental_order');
        $this->addSql('DROP TABLE rental_station');
    }
}
