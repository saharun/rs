<?php

namespace App\Controller;

use App\Contract\RentalStationForecastProviderInterface;
use App\Dto\RentalStationForecastRequest;
use App\Exception\StationNotFoundException;
use App\Service\RentalStationForecastRequestFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RentalStationForecastController extends AbstractController
{
    public function __construct(
        private RentalStationForecastRequestFactory $rentalStationForecastRequestFactory,
        private RentalStationForecastProviderInterface $rentalStationForecastProvider
    ) {
    }

    #[Route('/equipment-demand/forecast/{stationId}', name: 'equipment_demand_forecast_station')]
    public function getForecast(Request $request, int $stationId): Response
    {
        try {
            $rentalStationForecastRequest = $this->rentalStationForecastRequestFactory->fromHttpRequestAndStationId($request, $stationId);

            if (!$this->assertForecastRequestIsValid($rentalStationForecastRequest)) {
                return $this->json([], Response::HTTP_BAD_REQUEST);
            }

            $rentalStationForecast = $this->rentalStationForecastProvider->getForecastFor($rentalStationForecastRequest);
        } catch (StationNotFoundException $e) {
            return $this->json([], Response::HTTP_NOT_FOUND);
        } catch (\Exception $e) {
            return $this->json([], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->json($rentalStationForecast);
    }

    private function assertForecastRequestIsValid(RentalStationForecastRequest $equipmentDemandForecastRequest): bool
    {
        if ($equipmentDemandForecastRequest->getDateUntil() < $equipmentDemandForecastRequest->getDateFrom()) {
            return false;
        }

        $maxDateUntil = (new \DateTimeImmutable())->add(new \DateInterval('P1Y'));

        return $equipmentDemandForecastRequest->getDateUntil() < $maxDateUntil;
    }
}
