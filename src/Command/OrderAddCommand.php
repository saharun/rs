<?php

namespace App\Command;

use App\Dto\DayForecast;
use App\Entity\Equipment;
use App\Entity\EquipmentOrderItem;
use App\Entity\RentalOrder;
use App\Entity\RentalStation;
use App\Repository\EquipmentRepository;
use App\Repository\RentalOrderRepository;
use App\Repository\RentalStationRepository;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:order:add',
    description: 'Adds a given number of random orders to the database - for demo purposes only',
)]
class OrderAddCommand extends Command
{
    public function __construct(
        private RentalOrderRepository $rentalOrderRepository,
        private RentalStationRepository $rentalStationRepository,
        private EquipmentRepository $equipmentRepository
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption('number', null, InputOption::VALUE_OPTIONAL, 'Number of orders to add (positive integer), defaults to 1')
        ;
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $numberOfOrdersToAdd = intval($input->getOption('number') ?? 1);

        if ($numberOfOrdersToAdd) {
            $io->note(sprintf('%s orders will be created', $numberOfOrdersToAdd));
        }

        for ($i = 0; $i < $numberOfOrdersToAdd; ++$i) {
            $order = $this->createRandomOrder();
            $this->rentalOrderRepository->save($order);

            $io->success([
                sprintf('Order created for pickup %s:%s and return %s:%s',
                    $order->getPickupStation()->getName(),
                    $order->getPickupDate()->format(DayForecast::DAY_KEY_FORMAT),
                    $order->getReturnStation()->getName(),
                    $order->getReturnDate()->format(DayForecast::DAY_KEY_FORMAT)
                ),
                'EquipmentDemands:',
                array_reduce($order->getEquipmentItems()->toArray(), function ($value, $equipmentOrderItem) {
                    /* @var EquipmentOrderItem $equipmentOrderItem */
                    return $equipmentOrderItem->getEquipment()->getName().': '.$equipmentOrderItem->getCount().', '.$value;
                }, ''),
            ]);
        }

        return Command::SUCCESS;
    }

    /**
     * @throws Exception
     */
    private function createRandomOrder(): RentalOrder
    {
        $order = (new RentalOrder())
            ->setPickupDate((new \DateTimeImmutable())->add(new \DateInterval('P'.random_int(0, 30).'D')))
            ->setPickupStation($this->getRandomStation())
            ->setReturnStation($this->getRandomStation())
        ;
        $order->setReturnDate($order->getPickupDate()->add(new \DateInterval('P'.random_int(1, 10).'D')));
        $addedEquipmentIds = [];
        for ($i = 0; $i < random_int(1, 2); ++$i) {
            $equipmentOrderItem = new EquipmentOrderItem($this->getRandomEquipment(), random_int(1, 5));
            if (in_array($equipmentOrderItem->getEquipment()->getId(), $addedEquipmentIds)) {
                continue;
            }
            $addedEquipmentIds[] = $equipmentOrderItem->getEquipment()->getId();
            $order->addEquipmentItem(
                $equipmentOrderItem
            );
        }

        return $order;
    }

    /**
     * @throws Exception
     */
    private function getRandomStation(): RentalStation
    {
        $rentalStations = $this->rentalStationRepository->findAll();

        return $rentalStations[random_int(0, count($rentalStations) - 1)];
    }

    /**
     * @throws Exception
     */
    private function getRandomEquipment(): Equipment
    {
        $equipments = $this->equipmentRepository->findAll();

        return $equipments[random_int(0, count($equipments) - 1)];
    }
}
