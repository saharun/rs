<?php

namespace App\Contract;

use App\Dto\RentalStationForecast;
use App\Dto\RentalStationForecastRequest;

interface RentalStationForecastProviderInterface
{
    public function getForecastFor(RentalStationForecastRequest $rentalStationForecastRequest): RentalStationForecast;
}
