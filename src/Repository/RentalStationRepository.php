<?php

namespace App\Repository;

use App\Entity\RentalStation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RentalStation|null find($id, $lockMode = null, $lockVersion = null)
 * @method RentalStation|null findOneBy(array $criteria, array $orderBy = null)
 * @method RentalStation[]    findAll()
 * @method RentalStation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RentalStationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RentalStation::class);
    }
}
