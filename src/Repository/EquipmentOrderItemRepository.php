<?php

namespace App\Repository;

use App\Entity\EquipmentOrderItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EquipmentOrderItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method EquipmentOrderItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method EquipmentOrderItem[]    findAll()
 * @method EquipmentOrderItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EquipmentOrderItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EquipmentOrderItem::class);
    }
}
