<?php

namespace App\Repository;

use App\Entity\RentalOrder;
use App\Entity\RentalStation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RentalOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method RentalOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method RentalOrder[]    findAll()
 * @method RentalOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RentalOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RentalOrder::class);
    }

    public function save(RentalOrder $rentalOrder)
    {
        $this->_em->persist($rentalOrder);
        $this->_em->flush();
    }

    public function findByPickupStationAndDateRange(RentalStation $rentalStation, \DateTimeImmutable $dateFrom, \DateTimeImmutable $dateTo): array
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.pickupStation = :station')
            ->setParameter('station', $rentalStation)
            ->andWhere('r.pickupDate >= :dateFrom')
            ->setParameter('dateFrom', $dateFrom, Types::DATE_IMMUTABLE)
            ->andWhere('r.pickupDate <= :dateTo')
            ->setParameter('dateTo', $dateTo, Types::DATE_IMMUTABLE)
            ->orderBy('r.pickupDate', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByReturnStationAndDateRange(RentalStation $rentalStation, \DateTimeImmutable $dateFrom, \DateTimeImmutable $dateTo): array
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.returnStation = :station')
            ->setParameter('station', $rentalStation)
            ->andWhere('r.returnDate >= :dateFrom')
            ->setParameter('dateFrom', $dateFrom, Types::DATE_IMMUTABLE)
            ->andWhere('r.returnDate <= :dateTo')
            ->setParameter('dateTo', $dateTo, Types::DATE_IMMUTABLE)
            ->orderBy('r.returnDate', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }
}
