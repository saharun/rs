<?php

namespace App\Serializer\Normalizer;

use App\Dto\EquipmentStocks;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class EquipmentStocksNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    public function normalize($object, $format = null, array $context = []): array
    {
        $data = [];

        foreach ($object->getStockItems() as $stockItem) {
            $data[$stockItem->getEquipment()->getName()] = $stockItem->getStockCount();
        }

        return $data;
    }

    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof EquipmentStocks;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
