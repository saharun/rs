<?php

namespace App\Serializer\Normalizer;

use App\Dto\EquipmentChange;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class EquipmentChangeNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    public function normalize($object, $format = null, array $context = []): array
    {
        $data = [
            $object->getEquipment()->getName() => $object->getCount(),
        ];

        return $data;
    }

    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof EquipmentChange;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
