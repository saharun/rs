<?php

namespace App\Service;

use App\Dto\DayForecast;
use App\Dto\EquipmentChange;
use App\Dto\EquipmentChangesPerDay;
use App\Dto\EquipmentStock;
use App\Dto\EquipmentStocks;
use App\Dto\RentalStationForecast;
use App\Dto\RentalStationForecastRequest;
use App\Entity\EquipmentStock as EquipmentStockEntity;
use App\Entity\RentalOrder;
use Psr\Log\LoggerInterface;

class RentalStationForecastBuilder
{
    private ?RentalStationForecastRequest $rentalStationForecastRequest = null;

    /** @var array<RentalOrder> */
    private array $pickupOrders = [];

    /** @var array<RentalOrder> */
    private array $returnOrders = [];

    /** @var array<EquipmentStockEntity> */
    private array $initialStocks = [];

    public function __construct(private LoggerInterface $logger)
    {
    }

    public function forRentalStationForecastRequest(RentalStationForecastRequest $rentalStationForecastRequest): self
    {
        $this->rentalStationForecastRequest = $rentalStationForecastRequest;

        return $this;
    }

    /**
     * @param array<RentalOrder> $pickupOrders
     */
    public function forPickupOrders(array $pickupOrders): self
    {
        $this->pickupOrders = $pickupOrders;

        return $this;
    }

    /**
     * @param array<RentalOrder> $returnOrders
     */
    public function forReturnOrders(array $returnOrders): self
    {
        $this->returnOrders = $returnOrders;

        return $this;
    }

    /**
     * @param array<EquipmentStockEntity> $initialStocks
     */
    public function forInitialStocks(array $initialStocks): self
    {
        $this->initialStocks = $initialStocks;

        return $this;
    }

    public function build(): RentalStationForecast
    {
        if (!$this->canBuild()) {
            throw new \InvalidArgumentException('Cannot build RentalStationForecast due to invalid data.');
        }

        try {
            $rentalStationForecast = new RentalStationForecast();

            $equipmentDemandsPerDay = $this->buildEquipmentDemandsPerDay();
            $equipmentReturnsPerDay = $this->buildEquipmentReturnsPerDay();

            $currentStocks = $this->buildInitialStocks();
            $currentDay = $this->rentalStationForecastRequest->getDateFrom();

            while ($currentDay <= $this->rentalStationForecastRequest->getDateUntil()) {
                $currentEquipmentDemands = $equipmentDemandsPerDay->getEquipmentChangesForDay($currentDay);
                $currentEquipmentReturns = $equipmentReturnsPerDay->getEquipmentChangesForDay($currentDay);

                $dayForecast = new DayForecast(
                    $currentStocks,
                    $currentEquipmentDemands,
                    $currentEquipmentReturns
                );

                $rentalStationForecast->addDayForecast($currentDay, $dayForecast);

                $currentDay = $currentDay->add(new \DateInterval('P1D'));
                $currentStocks = $this->buildStocksFromPreviousStockAndEquipmentChanges(
                    $dayForecast->getEquipmentStocks(),
                    $currentEquipmentDemands,
                    $currentEquipmentReturns
                );
            }

            return $rentalStationForecast;
        } catch (\Throwable $e) {
            $this->logger->error('Error on building RentalStationForecast: '.$e->getMessage());

            return new RentalStationForecast();
        } finally {
            $this->rentalStationForecastRequest = null;
            $this->pickupOrders = [];
            $this->returnOrders = [];
            $this->initialStocks = [];
        }
    }

    private function canBuild(): bool
    {
        return null !== $this->rentalStationForecastRequest;
    }

    private function buildInitialStocks(): EquipmentStocks
    {
        $equipmentStocks = new EquipmentStocks();
        foreach ($this->initialStocks as $initialStock) {
            $equipmentStocks->addEquipmentStock(EquipmentStock::fromEntity($initialStock));
        }

        return $equipmentStocks;
    }

    private function buildEquipmentDemandsPerDay(): EquipmentChangesPerDay
    {
        $equipmentDemandsPerDay = new EquipmentChangesPerDay();

        foreach ($this->pickupOrders as $pickupOrder) {
            $equipmentDemandsPerDay->addEquipmentChangesForRentalOrderAndDay($pickupOrder, $pickupOrder->getPickupDate());
        }

        return $equipmentDemandsPerDay;
    }

    private function buildEquipmentReturnsPerDay(): EquipmentChangesPerDay
    {
        $equipmentReturnsPerDay = new EquipmentChangesPerDay();

        foreach ($this->returnOrders as $returnOrder) {
            $equipmentReturnsPerDay->addEquipmentChangesForRentalOrderAndDay($returnOrder, $returnOrder->getReturnDate());
        }

        return $equipmentReturnsPerDay;
    }

    /**
     * @param array<EquipmentChange> $equipmentDemands
     * @param array<EquipmentChange> $equipmentReturns
     */
    private function buildStocksFromPreviousStockAndEquipmentChanges(
        EquipmentStocks $equipmentStocks,
        array $equipmentDemands,
        array $equipmentReturns
    ): EquipmentStocks {
        $newStocks = new EquipmentStocks();
        foreach ($equipmentStocks->getStockItems() as $stockItem) {
            $newStocks->addEquipmentStock(clone $stockItem);
        }

        foreach ($equipmentDemands as $equipmentDemand) {
            $newStocks->updateStockByDemand($equipmentDemand);
        }

        foreach ($equipmentReturns as $equipmentReturn) {
            $newStocks->updateStockByReturn($equipmentReturn);
        }

        return $newStocks;
    }
}
