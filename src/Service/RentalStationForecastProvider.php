<?php

namespace App\Service;

use App\Contract\RentalStationForecastProviderInterface;
use App\Dto\RentalStationForecast;
use App\Dto\RentalStationForecastRequest;
use App\Entity\RentalStation;
use App\Exception\StationNotFoundException;
use App\Repository\RentalOrderRepository;
use App\Repository\RentalStationRepository;

class RentalStationForecastProvider implements RentalStationForecastProviderInterface
{
    public function __construct(
        private RentalStationForecastBuilder $rentalStationForecastBuilder,
        private RentalStationRepository $rentalStationRepository,
        private RentalOrderRepository $rentalOrderRepository
    ) {
    }

    public function getForecastFor(RentalStationForecastRequest $rentalStationForecastRequest): RentalStationForecast
    {
        $rentalStation = $this->rentalStationRepository->findOneBy(['id' => $rentalStationForecastRequest->getStationId()]);

        if (!$rentalStation instanceof RentalStation) {
            throw new StationNotFoundException(sprintf('There is no station with id %s', $rentalStationForecastRequest->getStationId()));
        }

        $pickupOrders = $this->rentalOrderRepository->findByPickupStationAndDateRange(
            $rentalStation,
            $rentalStationForecastRequest->getDateFrom(),
            $rentalStationForecastRequest->getDateUntil()
        );

        $returnOrders = $this->rentalOrderRepository->findByReturnStationAndDateRange(
            $rentalStation,
            $rentalStationForecastRequest->getDateFrom(),
            $rentalStationForecastRequest->getDateUntil()
        );

        $initialStocks = $rentalStation->getEquipmentStocks()->toArray();

        return $this->rentalStationForecastBuilder
            ->forRentalStationForecastRequest($rentalStationForecastRequest)
            ->forInitialStocks($initialStocks)
            ->forPickupOrders($pickupOrders)
            ->forReturnOrders($returnOrders)
            ->build();
    }
}
