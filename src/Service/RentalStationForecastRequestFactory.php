<?php

namespace App\Service;

use App\Dto\RentalStationForecastRequest;
use Symfony\Component\HttpFoundation\Request;

class RentalStationForecastRequestFactory
{
    public const DEFAULT_DATE_UNTIL_INTERVAL = 'P30D';

    public function fromHttpRequestAndStationId(Request $request, int $stationId): RentalStationForecastRequest
    {
        $dateFrom = new \DateTimeImmutable();
        $dateUntil = $this->extractDateUntilFrom($request) ?? (new \DateTimeImmutable())->add(new \DateInterval(self::DEFAULT_DATE_UNTIL_INTERVAL));

        $dateFrom = $dateFrom->setTime(0, 0, 0);
        $dateUntil = $dateUntil->setTime(23, 59, 59);

        return new RentalStationForecastRequest($stationId, $dateFrom, $dateUntil);
    }

    /**
     * @throws \Exception
     */
    private function extractDateUntilFrom(Request $request): ?\DateTimeImmutable
    {
        $dateUntilParam = $request->get('dateUntil');
        if (null === $dateUntilParam) {
            return null;
        }

        return new \DateTimeImmutable($dateUntilParam);
    }
}
