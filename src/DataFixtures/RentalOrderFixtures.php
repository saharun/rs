<?php

namespace App\DataFixtures;

use App\Entity\Equipment;
use App\Entity\EquipmentOrderItem;
use App\Entity\RentalOrder;
use App\Entity\RentalStation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class RentalOrderFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public function getDependencies()
    {
        return [
            EquipmentFixtures::class,
            RentalStationFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['test'];
    }

    public function load(ObjectManager $manager)
    {
        /** @var RentalStation $munich */
        $munich = $this->getReference(RentalStationFixtures::RENTAL_STATION_MUNICH_REFERENCE);
        /** @var RentalStation $berlin */
        $berlin = $this->getReference(RentalStationFixtures::RENTAL_STATION_BERLIN_REFERENCE);
        /** @var Equipment $equipmentOne */
        $equipmentOne = $this->getReference(EquipmentFixtures::EQUIPMENT_ONE_REFERENCE);
        /** @var Equipment $equipmentTwo */
        $equipmentTwo = $this->getReference(EquipmentFixtures::EQUIPMENT_TWO_REFERENCE);

        $datePickup = (new \DateTimeImmutable())->add(new \DateInterval('P1D'));
        $dateReturn = $datePickup->add(new \DateInterval('P4D'));

        $orderOne = (new RentalOrder())
            ->setPickupStation($munich)
            ->setPickupDate($datePickup)
            ->setReturnStation($berlin)
            ->setReturnDate($dateReturn)
            ->addEquipmentItem(new EquipmentOrderItem($equipmentOne, 2))
            ->addEquipmentItem(new EquipmentOrderItem($equipmentTwo, 3))
        ;

        $manager->persist($orderOne);

        $datePickup = (new \DateTimeImmutable())->add(new \DateInterval('P2D'));
        $dateReturn = $datePickup->add(new \DateInterval('P4D'));

        $orderTwo = (new RentalOrder())
            ->setPickupStation($munich)
            ->setPickupDate($datePickup)
            ->setReturnStation($berlin)
            ->setReturnDate($dateReturn)
            ->addEquipmentItem(new EquipmentOrderItem($equipmentOne, 1))
            ->addEquipmentItem(new EquipmentOrderItem($equipmentTwo, 5))
        ;

        $manager->persist($orderTwo);

        $datePickup = (new \DateTimeImmutable())->add(new \DateInterval('P1D'));
        $dateReturn = $datePickup->add(new \DateInterval('P2D'));

        $orderThree = (new RentalOrder())
            ->setPickupStation($berlin)
            ->setPickupDate($datePickup)
            ->setReturnStation($munich)
            ->setReturnDate($dateReturn)
            ->addEquipmentItem(new EquipmentOrderItem($equipmentOne, 2))
            ->addEquipmentItem(new EquipmentOrderItem($equipmentTwo, 2))
        ;

        $manager->persist($orderThree);

        $manager->flush();
    }
}
