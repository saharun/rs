<?php

namespace App\DataFixtures;

use App\Entity\Equipment;
use App\Entity\EquipmentStock;
use App\Entity\RentalStation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class RentalStationFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public const RENTAL_STATION_MUNICH_REFERENCE = 'rental-station-munich';
    public const RENTAL_STATION_BERLIN_REFERENCE = 'rental-station-berlin';

    public function getDependencies()
    {
        return [
            EquipmentFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['test', 'demo'];
    }

    public function load(ObjectManager $manager)
    {
        /** @var Equipment $equipmentOne */
        $equipmentOne = $this->getReference(EquipmentFixtures::EQUIPMENT_ONE_REFERENCE);
        /** @var Equipment $equipmentTwo */
        $equipmentTwo = $this->getReference(EquipmentFixtures::EQUIPMENT_TWO_REFERENCE);

        $munich = (new RentalStation('Munich'))
            ->addEquipmentStock(new EquipmentStock($equipmentOne, 50))
            ->addEquipmentStock(new EquipmentStock($equipmentTwo, 30))
        ;

        $manager->persist($munich);

        $berlin = (new RentalStation('Berlin'))
            ->addEquipmentStock(new EquipmentStock($equipmentOne, 30))
            ->addEquipmentStock(new EquipmentStock($equipmentTwo, 50))
        ;

        $manager->persist($berlin);

        $manager->flush();

        $this->setReference(self::RENTAL_STATION_MUNICH_REFERENCE, $munich);
        $this->setReference(self::RENTAL_STATION_BERLIN_REFERENCE, $berlin);
    }
}
