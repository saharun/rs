<?php

namespace App\DataFixtures;

use App\Entity\Equipment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class EquipmentFixtures extends Fixture implements FixtureGroupInterface
{
    public const EQUIPMENT_ONE_REFERENCE = 'equipment_one';
    public const EQUIPMENT_TWO_REFERENCE = 'equipment_two';

    public static function getGroups(): array
    {
        return ['test', 'demo'];
    }

    public function load(ObjectManager $manager)
    {
        $equipmentOne = new Equipment('Sleeping bag');
        $manager->persist($equipmentOne);

        $equipmentTwo = new Equipment('Bed sheets');
        $manager->persist($equipmentTwo);

        $manager->flush();

        $this->addReference(self::EQUIPMENT_ONE_REFERENCE, $equipmentOne);
        $this->addReference(self::EQUIPMENT_TWO_REFERENCE, $equipmentTwo);
    }
}
