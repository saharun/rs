<?php

namespace App\Dto;

use Symfony\Component\Serializer\Annotation\Ignore;

class EquipmentChange
{
    private Equipment $equipment;
    private int $count;

    public function __construct(Equipment $equipment, int $demandCount)
    {
        $this->equipment = $equipment;
        $this->count = $demandCount;
    }

    /**
     * @Ignore()
     */
    public function getEquipmentId(): int
    {
        return $this->getEquipment()->getId();
    }

    public function getEquipment(): Equipment
    {
        return $this->equipment;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function addToCount(int $add)
    {
        $this->count += $add;
    }
}
