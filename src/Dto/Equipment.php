<?php

namespace App\Dto;

use App\Entity\Equipment as EquipmentEntity;

class Equipment
{
    private int $id;
    private string $name;

    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public static function fromEntity(EquipmentEntity $equipmentEntity): self
    {
        return new self($equipmentEntity->getId(), $equipmentEntity->getName());
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
