<?php

namespace App\Dto;

class DayForecast
{
    public const DAY_KEY_FORMAT = 'Y-m-d';

    private EquipmentStocks $equipmentStocks;

    /** @var array<EquipmentChange> */
    private array $equipmentDemands = [];

    /** @var array<EquipmentChange> */
    private array $equipmentReturns = [];

    /**
     * @param EquipmentChange[] $equipmentDemands
     * @param EquipmentChange[] $equipmentReturns
     */
    public function __construct(EquipmentStocks $equipmentStocks, array $equipmentDemands, array $equipmentReturns)
    {
        $this->equipmentStocks = $equipmentStocks;
        $this->equipmentDemands = $equipmentDemands;
        $this->equipmentReturns = $equipmentReturns;
    }

    public function getEquipmentStocks(): EquipmentStocks
    {
        return $this->equipmentStocks;
    }

    public function setEquipmentStocks(EquipmentStocks $equipmentStocks): void
    {
        $this->equipmentStocks = $equipmentStocks;
    }

    /**
     * @return EquipmentChange[]
     */
    public function getEquipmentDemands(): array
    {
        return $this->equipmentDemands;
    }

    public function setEquipmentDemands(array $equipmentDemands)
    {
        $this->equipmentDemands = $equipmentDemands;
    }

    /**
     * @return EquipmentChange[]
     */
    public function getEquipmentReturns(): array
    {
        return $this->equipmentReturns;
    }

    /**
     * @param EquipmentChange[] $equipmentReturns
     */
    public function setEquipmentReturns(array $equipmentReturns): void
    {
        $this->equipmentReturns = $equipmentReturns;
    }
}
