<?php

namespace App\Dto;

class RentalStationForecast
{
    /**
     * @var array<DayForecast>
     */
    private array $dayForecasts = [];

    public function addDayForecast(\DateTimeImmutable $day, DayForecast $dailyForecast): self
    {
        $this->dayForecasts[$day->format(DayForecast::DAY_KEY_FORMAT)] = $dailyForecast;

        return $this;
    }

    /**
     * @return DayForecast[]
     */
    public function getDayForecasts(): array
    {
        return $this->dayForecasts;
    }
}
