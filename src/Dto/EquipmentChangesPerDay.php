<?php

namespace App\Dto;

use App\Entity\RentalOrder;

class EquipmentChangesPerDay
{
    /**
     * @var array<string, array<EquipmentChange>>
     */
    private array $equipmentChanges = [];

    public function addEquipmentChange(\DateTimeImmutable $day, EquipmentChange $equipmentChange): self
    {
        $existingEquipmentChange = $this->getExistingEquipmentChangeForDayAndEquipmentId($day, $equipmentChange->getEquipmentId());

        if (null === $existingEquipmentChange) {
            $this->equipmentChanges[$day->format(DayForecast::DAY_KEY_FORMAT)][] = $equipmentChange;

            return $this;
        }

        $existingEquipmentChange->addToCount($equipmentChange->getCount());

        return $this;
    }

    public function getEquipmentChangesForDay(\DateTimeImmutable $day): array
    {
        if (!array_key_exists($day->format(DayForecast::DAY_KEY_FORMAT), $this->equipmentChanges)) {
            return [];
        }

        return $this->equipmentChanges[$day->format(DayForecast::DAY_KEY_FORMAT)];
    }

    public function addEquipmentChangesForRentalOrderAndDay(RentalOrder $pickupOrder, \DateTimeImmutable $day): self
    {
        foreach ($pickupOrder->getEquipmentItems() as $orderEquipmentItem) {
            $this->addEquipmentChange(
                $day,
                new EquipmentChange(
                    Equipment::fromEntity($orderEquipmentItem->getEquipment()),
                    $orderEquipmentItem->getCount()
                )
            );
        }

        return $this;
    }

    private function getExistingEquipmentChangeForDayAndEquipmentId(\DateTimeImmutable $day, int $getEquipmentId)
    {
        if (!isset($this->equipmentChanges[$day->format(DayForecast::DAY_KEY_FORMAT)])) {
            return null;
        }

        $existingEquipmentChanges = $this->equipmentChanges[$day->format(DayForecast::DAY_KEY_FORMAT)];
        foreach ($existingEquipmentChanges as $existingEquipmentChange) {
            if ($existingEquipmentChange->getEquipmentId() === $getEquipmentId) {
                return $existingEquipmentChange;
            }
        }

        return null;
    }
}
