<?php

namespace App\Dto;

use App\Entity\EquipmentStock as EquipmentStockEntity;
use Symfony\Component\Serializer\Annotation\Ignore;

class EquipmentStock
{
    private Equipment $equipment;
    private int $stockCount;

    public function __construct(Equipment $equipment, int $stockCount)
    {
        $this->equipment = $equipment;
        $this->stockCount = $stockCount;
    }

    public static function fromEntity(EquipmentStockEntity $equipmentStock): self
    {
        return new self(
            Equipment::fromEntity($equipmentStock->getEquipment()),
            $equipmentStock->getCount()
        );
    }

    /**
     * @Ignore()
     */
    public function getEquipmentId(): int
    {
        return $this->getEquipment()->getId();
    }

    public function getEquipment(): Equipment
    {
        return $this->equipment;
    }

    public function getStockCount(): int
    {
        return $this->stockCount;
    }

    public function addToStockCount(int $stockCount): self
    {
        $this->stockCount += $stockCount;

        return $this;
    }

    public function removeFromStockCount(int $stockCount): self
    {
        $this->stockCount -= $stockCount;

        return $this;
    }
}
