<?php

namespace App\Dto;

class RentalStationForecastRequest
{
    private int $stationId;
    private \DateTimeImmutable $dateUntil;
    private \DateTimeImmutable $dateFrom;

    public function __construct(int $stationId, \DateTimeImmutable $dateFrom, \DateTimeImmutable $dateUntil)
    {
        $this->stationId = $stationId;
        $this->dateUntil = $dateUntil;
        $this->dateFrom = $dateFrom;
    }

    public function getStationId(): int
    {
        return $this->stationId;
    }

    public function getDateFrom(): \DateTimeImmutable
    {
        return $this->dateFrom;
    }

    public function getDateUntil(): \DateTimeImmutable
    {
        return $this->dateUntil;
    }
}
