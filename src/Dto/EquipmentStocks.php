<?php

namespace App\Dto;

class EquipmentStocks
{
    /**
     * @var array<EquipmentStock>
     */
    private array $stockItems = [];

    /**
     * @return EquipmentStock[]
     */
    public function getStockItems(): array
    {
        return $this->stockItems;
    }

    public function addEquipmentStock(EquipmentStock $equipmentStock): self
    {
        if (array_key_exists($equipmentStock->getEquipmentId(), $this->stockItems)) {
            $this->stockItems[$equipmentStock->getEquipmentId()]->addToStockCount($equipmentStock->getStockCount());
        } else {
            $this->stockItems[$equipmentStock->getEquipmentId()] = $equipmentStock;
        }

        return $this;
    }

    public function updateStockByDemand(EquipmentChange $equipmentDemand): self
    {
        if (array_key_exists($equipmentDemand->getEquipmentId(), $this->stockItems)) {
            $this->stockItems[$equipmentDemand->getEquipmentId()]->removeFromStockCount($equipmentDemand->getCount());
        } else {
            $this->stockItems[$equipmentDemand->getEquipmentId()] = new EquipmentStock($equipmentDemand->getEquipment(), -$equipmentDemand->getCount());
        }

        return $this;
    }

    public function updateStockByReturn(EquipmentChange $equipmentReturn): self
    {
        if (array_key_exists($equipmentReturn->getEquipmentId(), $this->stockItems)) {
            $this->stockItems[$equipmentReturn->getEquipmentId()]->addToStockCount($equipmentReturn->getCount());
        } else {
            $this->stockItems[$equipmentReturn->getEquipmentId()] = new EquipmentStock($equipmentReturn->getEquipment(), $equipmentReturn->getCount());
        }

        return $this;
    }

    public function getStockItemById(int $id): ?EquipmentStock
    {
        if (!array_key_exists($id, $this->stockItems)) {
            return null;
        }

        return $this->stockItems[$id];
    }
}
