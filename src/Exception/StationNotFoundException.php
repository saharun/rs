<?php

namespace App\Exception;

class StationNotFoundException extends \RuntimeException
{
}
