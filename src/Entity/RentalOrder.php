<?php

namespace App\Entity;

use App\Repository\RentalOrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RentalOrderRepository::class)]
class RentalOrder
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: RentalStation::class)]
    #[ORM\JoinColumn(nullable: false)]
    private RentalStation $pickupStation;

    #[ORM\ManyToOne(targetEntity: RentalStation::class)]
    #[ORM\JoinColumn(nullable: false)]
    private RentalStation $returnStation;

    #[ORM\OneToMany(mappedBy: 'rentalOrder', targetEntity: EquipmentOrderItem::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    /**
     * @var Collection<EquipmentOrderItem>
     */
    private Collection $equipmentItems;

    #[ORM\Column(type: 'date_immutable')]
    private \DateTimeImmutable $pickupDate;

    #[ORM\Column(type: 'date_immutable')]
    private \DateTimeImmutable $returnDate;

    public function __construct()
    {
        $this->equipmentItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPickupStation(): ?RentalStation
    {
        return $this->pickupStation;
    }

    public function setPickupStation(?RentalStation $pickupStation): self
    {
        $this->pickupStation = $pickupStation;

        return $this;
    }

    public function getReturnStation(): ?RentalStation
    {
        return $this->returnStation;
    }

    public function setReturnStation(?RentalStation $returnStation): self
    {
        $this->returnStation = $returnStation;

        return $this;
    }

    public function getEquipmentItems(): Collection
    {
        return $this->equipmentItems;
    }

    public function addEquipmentItem(EquipmentOrderItem $equipmentItem): self
    {
        if (!$this->equipmentItems->contains($equipmentItem)) {
            $this->equipmentItems[] = $equipmentItem;
            $equipmentItem->setRentalOrder($this);
        }

        return $this;
    }

    public function getPickupDate(): ?\DateTimeImmutable
    {
        return $this->pickupDate;
    }

    public function setPickupDate(\DateTimeImmutable $pickupDate): self
    {
        $this->pickupDate = $pickupDate;

        return $this;
    }

    public function getReturnDate(): ?\DateTimeImmutable
    {
        return $this->returnDate;
    }

    public function setReturnDate(\DateTimeImmutable $returnDate): self
    {
        $this->returnDate = $returnDate;

        return $this;
    }
}
