<?php

namespace App\Entity;

use App\Repository\EquipmentStockRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EquipmentStockRepository::class)]
class EquipmentStock
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Equipment::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Equipment $equipment;

    #[ORM\Column(type: 'integer')]
    private int $count;

    #[ORM\ManyToOne(targetEntity: RentalStation::class, inversedBy: 'equipmentStocks')]
    #[ORM\JoinColumn(nullable: false)]
    private RentalStation $rentalStation;

    public function __construct(Equipment $equipment, int $count)
    {
        $this->equipment = $equipment;
        $this->count = $count;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEquipment(): ?Equipment
    {
        return $this->equipment;
    }

    public function setEquipment(?Equipment $equipment): self
    {
        $this->equipment = $equipment;

        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function getRentalStation(): ?RentalStation
    {
        return $this->rentalStation;
    }

    public function setRentalStation(?RentalStation $rentalStation): self
    {
        $this->rentalStation = $rentalStation;

        return $this;
    }
}
