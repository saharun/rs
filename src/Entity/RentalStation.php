<?php

namespace App\Entity;

use App\Repository\RentalStationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RentalStationRepository::class)]
class RentalStation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[ORM\OneToMany(mappedBy: 'rentalStation', targetEntity: EquipmentStock::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    /**
     * @var Collection<EquipmentStock>
     */
    private Collection $equipmentStocks;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->equipmentStocks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEquipmentStocks(): Collection
    {
        return $this->equipmentStocks;
    }

    public function addEquipmentStock(EquipmentStock $equipmentStock): self
    {
        if (!$this->equipmentStocks->contains($equipmentStock)) {
            $this->equipmentStocks[] = $equipmentStock;
            $equipmentStock->setRentalStation($this);
        }

        return $this;
    }

    public function removeEquipmentStock(EquipmentStock $equipmentStock): self
    {
        if ($this->equipmentStocks->removeElement($equipmentStock)) {
            // set the owning side to null (unless already changed)
            if ($equipmentStock->getRentalStation() === $this) {
                $equipmentStock->setRentalStation(null);
            }
        }

        return $this;
    }
}
