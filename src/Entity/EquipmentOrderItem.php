<?php

namespace App\Entity;

use App\Repository\EquipmentOrderItemRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EquipmentOrderItemRepository::class)]
class EquipmentOrderItem
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Equipment::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Equipment $equipment;

    #[ORM\Column(type: 'integer')]
    private int $count;

    #[ORM\ManyToOne(targetEntity: RentalOrder::class, inversedBy: 'equipmentItems')]
    #[ORM\JoinColumn(nullable: false)]
    private RentalOrder $rentalOrder;

    public function __construct($equipment, $count)
    {
        $this->equipment = $equipment;
        $this->count = $count;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEquipment(): ?Equipment
    {
        return $this->equipment;
    }

    public function setEquipment(?Equipment $equipment): self
    {
        $this->equipment = $equipment;

        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function getRentalOrder(): ?RentalOrder
    {
        return $this->rentalOrder;
    }

    public function setRentalOrder(?RentalOrder $rentalOrder): self
    {
        $this->rentalOrder = $rentalOrder;

        return $this;
    }
}
