<?php

namespace App\Tests\Unit\Service;

use App\Dto\DayForecast;
use App\Dto\RentalStationForecast;
use App\Dto\RentalStationForecastRequest;
use App\Entity\Equipment;
use App\Entity\EquipmentOrderItem;
use App\Entity\EquipmentStock;
use App\Entity\RentalOrder;
use App\Entity\RentalStation;
use App\Service\RentalStationForecastBuilder;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class RentalStationForecastBuilderTest extends TestCase
{
    /**
     * @test
     */
    public function buildWithInvalidInputThrowsException()
    {
        self::expectException(\InvalidArgumentException::class);
        $logger = $this->createMock(LoggerInterface::class);

        $rentalStationForecast = (new RentalStationForecastBuilder($logger))
            ->build()
        ;
    }

    /**
     * @test
     */
    public function buildingWithValidInputDataReturnsForecast()
    {
        $rentalStation = $this->createMock(RentalStation::class);
        $rentalStation
            ->expects($this->any())
            ->method('getId')
            ->willReturn(1)
        ;

        $equipments = $this->createEquipments();
        $pickupOrders = [
            $this->createPickupOrder($rentalStation, new \DateTimeImmutable(), $equipments[0], 2),
            $this->createPickupOrder($rentalStation, (new \DateTimeImmutable())->add(new \DateInterval('P2D')), $equipments[1], 3),
        ];

        $returnOrders = [
            $this->createReturnOrder($rentalStation, new \DateTimeImmutable(), $equipments[0], 3),
            $this->createReturnOrder($rentalStation, (new \DateTimeImmutable())->add(new \DateInterval('P1D')), $equipments[1], 1),
        ];

        $equipmentStocks = [
            $this->createEquipmentStock($equipments[0], 10),
            $this->createEquipmentStock($equipments[1], 5),
        ];

        $dateFrom = new \DateTimeImmutable();
        $dateTo = $dateFrom->add(new \DateInterval('P5D'));

        $rentalStationForecastRequest = new RentalStationForecastRequest(
            $rentalStation->getId(),
            $dateFrom,
            $dateTo
        );

        $logger = $this->createMock(LoggerInterface::class);
        $rentalStationForecast = (new RentalStationForecastBuilder($logger))
            ->forRentalStationForecastRequest($rentalStationForecastRequest)
            ->forPickupOrders($pickupOrders)
            ->forReturnOrders($returnOrders)
            ->forInitialStocks($equipmentStocks)
            ->build()
        ;

        self::assertInstanceOf(RentalStationForecast::class, $rentalStationForecast);
        self::assertCount(6, $rentalStationForecast->getDayForecasts());
        $dateFromForecast = $rentalStationForecast->getDayForecasts()[$dateFrom->format(DayForecast::DAY_KEY_FORMAT)];
        self::assertEquals(10, $dateFromForecast->getEquipmentStocks()->getStockItemById(1)->getStockCount());
        self::assertEquals(2, $dateFromForecast->getEquipmentDemands()[0]->getCount());
        self::assertEquals(3, $dateFromForecast->getEquipmentReturns()[0]->getCount());

        $dateFromP1D = $dateFrom->add(new \DateInterval('P1D'));
        $dateFromP1DForecast = $rentalStationForecast->getDayForecasts()[$dateFromP1D->format(DayForecast::DAY_KEY_FORMAT)];
        self::assertEquals(11, $dateFromP1DForecast->getEquipmentStocks()->getStockItemById(1)->getStockCount());

        $dateToForecast = $rentalStationForecast->getDayForecasts()[$dateTo->format(DayForecast::DAY_KEY_FORMAT)];
        self::assertEquals(11, $dateToForecast->getEquipmentStocks()->getStockItemById(1)->getStockCount());
        self::assertEquals(3, $dateToForecast->getEquipmentStocks()->getStockItemById(2)->getStockCount());
    }

    private function createPickupOrder(RentalStation $rentalStation, \DateTimeImmutable $pickupDate, Equipment $equipment, int $count): RentalOrder
    {
        return (new RentalOrder())
            ->setPickupStation($rentalStation)
            ->setPickupDate($pickupDate)
            ->addEquipmentItem(new EquipmentOrderItem($equipment, $count))
        ;
    }

    private function createReturnOrder(RentalStation $rentalStation, \DateTimeImmutable $returnDate, Equipment $equipment, int $count): RentalOrder
    {
        return (new RentalOrder())
            ->setReturnStation($rentalStation)
            ->setReturnDate($returnDate)
            ->addEquipmentItem(new EquipmentOrderItem($equipment, $count))
            ;
    }

    private function createEquipmentStock(Equipment $equipment, int $count): EquipmentStock
    {
        return new EquipmentStock($equipment, $count);
    }

    /**
     * @return Equipment[]
     */
    private function createEquipments(): array
    {
        return [
            $this->createEquipmentMock(1, 'Bed sheets'),
            $this->createEquipmentMock(2, 'Sleeping bag'),
        ];
    }

    private function createEquipmentMock(int $id, string $name): Equipment
    {
        $equipment = $this->createMock(Equipment::class);

        $equipment
            ->expects($this->any())
            ->method('getId')
            ->willReturn($id)
        ;

        $equipment
            ->expects($this->any())
            ->method('getName')
            ->willReturn($name)
        ;

        return $equipment;
    }
}
