<?php

namespace App\Tests\Unit\Service;

use App\Dto\RentalStationForecastRequest;
use App\Service\RentalStationForecastRequestFactory;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class RentalStationForecastRequestFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function creatingWithValidDateUntilHasCorrectDates()
    {
        $dateFrom = new \DateTimeImmutable();
        $dateUntil = $dateFrom->add(new \DateInterval('P15D'));

        $subject = new RentalStationForecastRequestFactory();
        $httpRequest = new Request();
        $httpRequest->query->set('dateUntil', $dateUntil->format('Y-m-d'));

        $request = $subject->fromHttpRequestAndStationId($httpRequest, 1);

        self::assertInstanceOf(RentalStationForecastRequest::class, $request);
        self::assertEquals($dateFrom->format('Y-m-d'), $request->getDateFrom()->format('Y-m-d'));
        self::assertEquals($dateUntil->format('Y-m-d'), $request->getDateUntil()->format('Y-m-d'));
    }

    /**
     * @test
     */
    public function creatingWithoutDateUntilUsesDefault()
    {
        $subject = new RentalStationForecastRequestFactory();
        $httpRequest = new Request();

        $request = $subject->fromHttpRequestAndStationId($httpRequest, 1);

        self::assertInstanceOf(RentalStationForecastRequest::class, $request);
        self::assertEquals($this->getDefaultDateUntil()->format('Y-m-d'), $request->getDateUntil()->format('Y-m-d'));
    }

    /**
     * @test
     */
    public function creatingWithInvalidDateUntilThrowsException()
    {
        $dateUntil = (new \DateTimeImmutable())->add(new \DateInterval('P15D'));

        $subject = new RentalStationForecastRequestFactory();
        $httpRequest = new Request();
        $httpRequest->query->set('dateUntil', $dateUntil->format('m-d'));

        self::expectException(\Exception::class);

        $subject->fromHttpRequestAndStationId($httpRequest, 1);
    }

    private function getDefaultDateUntil(): \DateTimeImmutable
    {
        return (new \DateTimeImmutable())->add(new \DateInterval(RentalStationForecastRequestFactory::DEFAULT_DATE_UNTIL_INTERVAL));
    }
}
