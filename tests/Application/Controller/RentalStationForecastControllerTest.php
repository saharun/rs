<?php

namespace App\Tests\Application\Controller;

use App\DataFixtures\RentalOrderFixtures;
use App\DataFixtures\RentalStationFixtures;
use App\Entity\RentalStation;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @coversDefaultClass \App\Controller\RentalStationForecastController
 */
class RentalStationForecastControllerTest extends WebTestCase
{
    public const EQUIPMENT_DEMAND_FORECAST_URL = '/equipment-demand/forecast/%s';

    /**
     * @test
     * @covers ::getForecast
     * @dataProvider getInvalidDateUntilValues
     */
    public function requestWithInvalidDateUntilReturnsBadRequest(\DateTimeImmutable $dateUntil): void
    {
        $client = static::createClient();
        $databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();
        $fixtures = $databaseTool->loadFixtures([
            RentalOrderFixtures::class,
        ])->getReferenceRepository();

        /** @var RentalStation $rentalStationMunich */
        $rentalStationMunich = $fixtures->getReference(RentalStationFixtures::RENTAL_STATION_MUNICH_REFERENCE);

        $crawler = $client->request(
            'GET',
            sprintf(self::EQUIPMENT_DEMAND_FORECAST_URL, $rentalStationMunich->getId()),
            [
                'dateUntil' => $dateUntil->format('Y-m-d'),
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
    }

    public function getInvalidDateUntilValues(): array
    {
        return [
            [(new \DateTimeImmutable('-1 day'))],
            [(new \DateTimeImmutable('1 year'))],
        ];
    }

    /**
     * @test
     * @covers ::getForecast
     */
    public function requestWithInValidStationReturnsNotFound(): void
    {
        $client = static::createClient();
        $databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();
        $fixtures = $databaseTool->loadFixtures([
            RentalOrderFixtures::class,
        ])->getReferenceRepository();

        $crawler = $client->request('GET', sprintf(self::EQUIPMENT_DEMAND_FORECAST_URL, 3));

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    /**
     * @test
     * @covers ::getForecast
     */
    public function requestWithValidStationReturnsDefaultForecast(): void
    {
        $client = static::createClient();
        $databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();
        $fixtures = $databaseTool->loadFixtures([
            RentalOrderFixtures::class,
        ])->getReferenceRepository();

        /** @var RentalStation $rentalStationMunich */
        $rentalStationMunich = $fixtures->getReference(RentalStationFixtures::RENTAL_STATION_MUNICH_REFERENCE);

        $crawler = $client->request('GET', sprintf(self::EQUIPMENT_DEMAND_FORECAST_URL, $rentalStationMunich->getId()));

        $this->assertResponseIsSuccessful();
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        self::assertArrayHasKey('dayForecasts', $content);
        self::assertCount(31, $content['dayForecasts']);
    }
}
