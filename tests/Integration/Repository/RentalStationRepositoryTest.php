<?php

namespace App\Tests\Integration\Repository;

use App\DataFixtures\RentalStationFixtures;
use App\Entity\RentalStation;
use App\Repository\RentalStationRepository;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Liip\TestFixturesBundle\Services\DatabaseTools\AbstractDatabaseTool;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RentalStationRepositoryTest extends KernelTestCase
{
    private RentalStationRepository $subject;
    private AbstractDatabaseTool $databaseTool;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->subject = self::getContainer()->get(RentalStationRepository::class);
        $this->databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();
    }

    /**
     * @test
     */
    public function findsRentalStationByName(): void
    {
        $fixtures = $this->databaseTool->loadFixtures([
            RentalStationFixtures::class,
        ])->getReferenceRepository();

        /** @var RentalStation $munich */
        $munich = $fixtures->getReference(RentalStationFixtures::RENTAL_STATION_MUNICH_REFERENCE);

        $rentalStation = $this->subject->findOneBy(['name' => $munich->getName()]);

        self::assertSame($munich, $rentalStation);
    }
}
