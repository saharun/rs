<?php

namespace App\Tests\Integration\Repository;

use App\DataFixtures\RentalOrderFixtures;
use App\DataFixtures\RentalStationFixtures;
use App\Entity\RentalOrder;
use App\Repository\RentalOrderRepository;
use Doctrine\DBAL\Connection;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Liip\TestFixturesBundle\Services\DatabaseTools\AbstractDatabaseTool;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RentalOrderRepositoryTest extends KernelTestCase
{
    private RentalOrderRepository $subject;
    private AbstractDatabaseTool $databaseTool;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->subject = self::getContainer()->get(RentalOrderRepository::class);
        $this->databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();
    }

    /**
     * @test
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function savesOrderSuccessfully()
    {
        $fixtures = $this->databaseTool->loadFixtures([
            RentalStationFixtures::class,
        ])->getReferenceRepository();

        $munich = $fixtures->getReference(RentalStationFixtures::RENTAL_STATION_MUNICH_REFERENCE);

        $dateFrom = new \DateTimeImmutable();
        $dateTo = $dateFrom->add(new \DateInterval('P10D'));

        $order = (new RentalOrder())
            ->setPickupStation($munich)
            ->setPickupDate($dateFrom)
            ->setReturnStation($munich)
            ->setReturnDate($dateTo)
            ;

        $this->subject->save($order);

        /** @var Connection $db */
        $db = self::getContainer()->get(Connection::class);
        $stmt = $db->prepare('SELECT * FROM rental_order WHERE id = :id');
        $stmt->bindValue('id', $order->getId());
        $rentalOrderData = $stmt->executeQuery()->fetchAssociative();

        self::assertEquals(1, $rentalOrderData['id']);
    }

    /**
     * @test
     */
    public function findsRentalOrdersByPickupStationAndDateRange(): void
    {
        $fixtures = $this->databaseTool->loadFixtures([
            RentalOrderFixtures::class,
        ])->getReferenceRepository();

        $munich = $fixtures->getReference(RentalStationFixtures::RENTAL_STATION_MUNICH_REFERENCE);

        $dateFrom = new \DateTimeImmutable();
        $dateTo = $dateFrom->add(new \DateInterval('P10D'));

        $pickupOrders = $this->subject->findByPickupStationAndDateRange(
            $munich,
            $dateFrom,
            $dateTo
        );

        self::assertCount(2, $pickupOrders);
    }

    /**
     * @test
     */
    public function findsRentalOrdersByReturnStationAndDateRange(): void
    {
        $fixtures = $this->databaseTool->loadFixtures([
            RentalOrderFixtures::class,
        ])->getReferenceRepository();

        $munich = $fixtures->getReference(RentalStationFixtures::RENTAL_STATION_MUNICH_REFERENCE);

        $dateFrom = new \DateTimeImmutable();
        $dateTo = $dateFrom->add(new \DateInterval('P10D'));

        $returnOrders = $this->subject->findByReturnStationAndDateRange(
            $munich,
            $dateFrom,
            $dateTo
        );

        self::assertCount(1, $returnOrders);
    }
}
