# Usage
# make 				# Builds, installs, loads fixtures and starts an image with a local web server
# make add_order 	# Adds 5 random orders to the database
# make test			# Runs phpunit, cs-fixer and phpstan

.DEFAULT_GOAL := serve
.PHONY: build install attach serve test load add_order

build:
	@docker build ./ -f _dev/php/Dockerfile \
		--build-arg USER_ID=$(shell id -u) \
		--build-arg GROUP_ID=$(shell id -g) \
		--build-arg GIT_USER_EMAIL="$(shell git config user.email)" \
		--build-arg GIT_USER_NAME="$(shell git config user.name)" \
		-t rs-coding-task:latest

install:
	@docker run --rm --interactive --tty --name rs-coding-task-install \
		--volume $(PWD):/project \
		--workdir="/project" \
		--user=user \
			rs-coding-task bash -c "composer install --no-scripts"

load:
	@docker run --rm --interactive --tty --name rs-coding-task-load \
		--volume $(PWD):/project \
		--workdir="/project" \
		--user=user \
		rs-coding-task bash -c \
			"bin/console doctrine:schema:drop --force \
			&& bin/console doctrine:schema:update --force \
			&& bin/console doctrine:fixtures:load -n --group=demo"


serve: build install load
	@docker run --rm --interactive --tty --name rs-coding-task-serve \
		--volume $(PWD):/project \
		--workdir="/project" \
		-p 8000:8000 \
		--user=user \
		-e PHP_IDE_CONFIG="serverName=symfony.wip" \
		rs-coding-task bash -c \
			"symfony serve"

attach:
	@docker run --rm --interactive --tty --name rs-coding-task-dev \
		--volume $(PWD):/project \
		--workdir="/project" \
		--user=user \
		rs-coding-task bash

add_order:
	@docker run --rm --interactive --tty --name rs-coding-task-add-order \
		--volume $(PWD):/project \
		--workdir="/project" \
		--user=user \
		rs-coding-task bash -c \
			"bin/console app:order:add --number=5"

test: build install
	@docker run --rm --interactive --tty --name rs-coding-task-test \
		--volume $(PWD):/project \
		--workdir="/project" \
		--user=user \
		rs-coding-task bash -c \
			"vendor/bin/php-cs-fixer fix --dry-run\
			&& vendor/bin/phpstan --level=3 analyse src tests \
			&& php bin/phpunit"