# Coding task for RS

## Local setup
In order to run the project locally you have to clone it and then run the following command in the project directory (needs docker and make):

```
make
```

This will basically 

* build a docker image with PHP8.1, composer and the symfony-cli
* perform a composer install
* create the database with some fixtures for rental stations and equipments (sqlite)
* starts an image with a local web server (default port is 8000)

Hint: Running make again, will reset the database to the default fixture set.

## Demo

After running the make command, the equipment demand endpoint is available under the following url:

<a href="http://localhost:8000/equipment-demand/forecast/1" target="_blank">http://localhost:8000/equipment-demand/forecast/{stationId} </a>

The initial fixtures provide two stations with id 1 (Munich) and 2 (Berlin).

The response will show a list of day forecasts for the next 30 days. To change the number of days of the forecasts,
the parameter dateUntil can be passed with the url in the format Y-m-d (2022-03-01). Each entry contains the 
current stocks and the equipment demands and equipment returns that happen on that day.

In order to simulate some orders, the following command can be used: 

```
make add_order
```

It creates 5 random orders on each invocation in the database.

Finally, you can use the following make command to run the unit tests, cs-fixer and phpstan:  

```
make test
```